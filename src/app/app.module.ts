import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './navigation/header/header.component';
import { SidenavComponent } from './navigation/sidenav/sidenav.component';
import { HomeComponent } from './home/home.component';
import { ShelterComponent } from './shelter/shelter.component';
import { LoginComponent } from './auth/login/login.component';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { APIService } from './api.service';
import { AuthService } from './auth.service';
import { HttpClientModule } from '@angular/common/http';
import { AnimalComponent } from './animal/animal.component';
import { DialogBoxComponent } from './animal/dialog-box/dialog-box.component';
import { BranchComponent } from './branch/branch.component';
import { BranchDialogBoxComponent } from './branch/branch-dialog-box/branch-dialog-box.component'; 

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidenavComponent,
    HomeComponent,
    ShelterComponent,
    LoginComponent,
    AnimalComponent,
    DialogBoxComponent,
    BranchComponent,
    BranchDialogBoxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule
  ],
  entryComponents: [
    DialogBoxComponent,
    BranchDialogBoxComponent
  ],
  providers: [
    APIService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

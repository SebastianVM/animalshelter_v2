import { Component, OnInit } from '@angular/core';
import { APIService } from '../api.service';

@Component({
  selector: 'app-shelter',
  templateUrl: './shelter.component.html',
  styleUrls: ['./shelter.component.css']
})
export class ShelterComponent implements OnInit {

  shelterAnimals: [] = [];

  constructor(private apiService: APIService) { }

  ngOnInit() {
    this.apiService.getShelterAnimals()
    .subscribe((animals: []) => {
      console.log(animals);
      this.shelterAnimals = animals;
    });
  }

}

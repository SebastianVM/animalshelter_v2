import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class APIService {
    shelterListChanged = new Subject<any>();
    shelterAniamls: [] = [];
    APIEndPoint = 'https://eyavqewaud.execute-api.ap-northeast-1.amazonaws.com/production/animal';
    // APIEndPoint = "production/animal";
    // BranchAPIEndPoint= "production/branch";
    BranchAPIEndPoint = 'https://eyavqewaud.execute-api.ap-northeast-1.amazonaws.com/production/branch';

    constructor(private http: HttpClient) {}

    getShelterAnimals() {
        return this.http.get(this.APIEndPoint);
    }

    postShelterAnimal(animal) {
        return this.http.post(this.APIEndPoint, animal);
    }

    putShelterAnimal(animal) {
        return this.http.put(this.APIEndPoint, animal);
    }

    deleteShelterAnimal(animal) {
        const URL = `${this.APIEndPoint}/${animal}`;
        return this.http.delete(URL);
    }

    // Branch methods
    getBranches() {
        return this.http.get(this.BranchAPIEndPoint);
    }

    postBranch(branch) {
        return this.http.post(this.BranchAPIEndPoint, branch);
    }

    putBranch(branch) {
        return this.http.put(this.BranchAPIEndPoint, branch);
    }
}
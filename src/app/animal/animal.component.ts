import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTable, MatDialog, MatTableDataSource, MatSort, MatSnackBar } from '@angular/material';
import { DialogBoxComponent } from './dialog-box/dialog-box.component';
import { APIService } from '../api.service';

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.css']
})
export class AnimalComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['id', 'name', 'kind', 'Entry date', 'Edit', 'Delete'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatTable, {static: true}) table: MatTable<any>;
  @ViewChild(MatSort, null) sort: MatSort;

  constructor(public dialog: MatDialog, private apiService: APIService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.refreshDataSource();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  refreshDataSource() {
    this.apiService.getShelterAnimals()
    .subscribe((animals: []) => {
      console.log(animals);
      this.dataSource.data = animals;
    });
  }

  openDialog(action, obj: any) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '400px',
      data: obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event === 'Add') {
        this.addRowData(result.data);
      } else if (result.event === 'Update') {
        this.updateRowData(result.data);
      } else if (result.event === 'Delete') {
        this.deleteRowData(result.data);
      }
    });
  }

  addRowData(rowObj: any) {
    const createdOn = new Date();
    const animal  = {
      animal_name: rowObj.animal_name,
      animal_kind: rowObj.animal_kind,
      animal_entry_date: rowObj.animal_entry_date,
      animal_age: rowObj.animal_age,
      animal_created_on: createdOn,
      branch_id: rowObj.branch_id,
      animal_status_id: rowObj.animal_status_id
    };

    this.apiService.postShelterAnimal(animal).subscribe(responce => {
      console.log(responce);
      this.refreshDataSource();
      this.snackBar.open('Animal added succesfully', 'close', {
        duration: 3000
      });
    },
    error => {
      this.snackBar.open('An error has ocurred, please contact an intern', 'close', {
        duration: 3000
      });
    });
  }

  updateRowData(rowObj: any) {
    const animal  = {
      animal_id: rowObj.animal_id,
      animal_name: rowObj.animal_name,
      animal_kind: rowObj.animal_kind,
      animal_entry_date: rowObj.animal_entry_date,
      animal_age: rowObj.animal_age,
      animal_created_on: rowObj.animal_created_on,
      branch_id: rowObj.branch_id,
      animal_status_id: rowObj.animal_status_id
    };

    this.apiService.putShelterAnimal(animal).subscribe(responce => {
      console.log(responce);
      this.refreshDataSource();
      this.snackBar.open('Animal updated succenfully', 'close', {
        duration: 3000
      });
    },
    error => {
      this.snackBar.open('An error has ocurred, please contact an intern', 'close', {
        duration: 3000
      });
    });
  }

  deleteRowData(rowObj) {
    this.apiService.deleteShelterAnimal(rowObj.animal_id).subscribe(responce => {
      console.log(responce);
      this.refreshDataSource();
      this.snackBar.open('Animal deleted succesfully', 'close', {
        duration: 3000
      });
    },
    error => {
      this.snackBar.open('An error has ocurred, please contact an intern', 'close', {
        duration: 3000
      });
    });
  }

}

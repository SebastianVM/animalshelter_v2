import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'gameCenter';
​
  constructor(private http: HttpClient) {}

  teams$ = this.http.get('/api/teams');

  createTeam() {
    console.log('call');

    const team = {
      id: null,
      name: 'ANY',
      coach: 'ANY',
      description: 'ANY'
    };

    console.log(team);
    this.http.post('/api/teams', team);
  }

  deleteTeam(id: number) {
    const URL = 'api/teams/' + id;
    console.log(URL);
    return this.http.delete(URL);
  }
}
